package main

import "fmt"
// go run will not output a binary
// go build will output a binary
func main() {
	fmt.Println("Hello World")
}